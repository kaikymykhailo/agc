library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity adder_sinus_and_noise is
Generic(
    WIDTH_OUTPUT : natural;
    WIDTH_SINUS : natural;
    WIDTH_NOISE : natural
);
PORT( 
    noise : in std_logic_vector(WIDTH_NOISE-1 downto 0);
    sinus : in std_logic_vector(WIDTH_SINUS-1 downto 0);
    output : out std_logic_vector(WIDTH_OUTPUT-1 downto 0)
);
end adder_sinus_and_noise;

architecture Behavioral of adder_sinus_and_noise is
begin
	process(noise, sinus)
	variable result : signed(WIDTH_OUTPUT-1 downto 0);
	variable tmp_result : signed(WIDTH_OUTPUT downto 0);
	begin
	result := (others => '0');
	tmp_result := resize(SIGNED(noise), WIDTH_OUTPUT+1) + resize(SIGNED(sinus), WIDTH_OUTPUT+1);
	if tmp_result(WIDTH_OUTPUT) /= tmp_result(WIDTH_OUTPUT-1) then
		result(WIDTH_OUTPUT-1) := tmp_result(WIDTH_OUTPUT);
		result(WIDTH_OUTPUT-2 downto 0) := (others => tmp_result(WIDTH_OUTPUT-1));
	else
		result := tmp_result(WIDTH_OUTPUT-1 downto 0);
	end if;
	output <= std_logic_vector(result);
	end process;
end Behavioral;
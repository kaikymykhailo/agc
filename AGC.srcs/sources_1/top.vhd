library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top is
Generic(
	N : natural;
	WIDTH_DATA_NOISE : natural
);
Port ( 
    clk : in std_logic;
    reset : in std_logic;
	switch_signals : in std_logic_vector(1 downto 0);
	factor : in signed(N-1 downto 0);
	alpha : in unsigned(N-1 downto 0);
	R : in signed(N-1 downto 0);
    output_data : out signed(N-1 downto 0)
);
end top;

architecture Behavioral of top is


component sinusGenerating 
Generic(N : natural);
Port (
    clk : in std_logic;
    reset : in std_logic;
    sin_factor : in signed(N-1 downto 0);
    output_data : out std_logic_vector(N-1 downto 0)        
);
end component;

component AGC
Generic(N : natural);
Port(
	clk : in std_logic;
	reset : in std_logic;
	data_input : in signed(N-1 downto 0);
	R : in signed(N-1 downto 0);
	alpha : in unsigned(N-1 downto 0);
	data_output : out signed(N-1 downto 0)
);
end component;

component WhiteNoiseGen is
Generic(
    N : natural;
	OUTPUT_WIDTH_NOISE : natural
);
PORT( 
	clk : in std_logic;
	reset : in std_logic;
	output : out std_logic_vector(N-1 downto 0)
);
end component;

component adder_sinus_and_noise
Generic(
    WIDTH_OUTPUT : natural;
    WIDTH_SINUS : natural;
    WIDTH_NOISE : natural
);
PORT( 
    noise : in std_logic_vector(WIDTH_NOISE-1 downto 0);
    sinus : in std_logic_vector(WIDTH_SINUS-1 downto 0);
    output : out std_logic_vector(WIDTH_OUTPUT-1 downto 0)
);
end component;

-- Signals -- 
signal output_data_sinus_generator : std_logic_vector(N-1 downto 0);
signal output_data_noise_generator : std_logic_vector(N-1 downto 0);
signal output_data_adder_noize_and_sinus : std_logic_vector(N-1 downto 0);

signal tmp_input_AGC : std_logic_vector(N-1 downto 0);

-- End signals --

begin
process(clk)
begin
case switch_signals is
when "00" => tmp_input_AGC <= output_data_sinus_generator;
when "11" => tmp_input_AGC <= output_data_noise_generator;
when others => tmp_input_AGC <= output_data_adder_noize_and_sinus;
end case;
end process;

module_adder : adder_sinus_and_noise Generic Map(WIDTH_OUTPUT => N, WIDTH_SINUS => N, WIDTH_NOISE => N) Port Map(noise => output_data_noise_generator, sinus => output_data_sinus_generator, output => output_data_adder_noize_and_sinus);
module_sinus_generate : sinusGenerating  Generic Map (N => N) Port Map(clk => clk, reset => reset, output_data => output_data_sinus_generator, sin_factor => factor);
module_WhiteNoiseGen: WhiteNoiseGen Generic Map (N=>N,OUTPUT_WIDTH_NOISE => WIDTH_DATA_NOISE) Port Map(clk => clk, reset => reset, output => output_data_noise_generator);
module_AGC : AGC Generic Map(N=>N) Port Map(clk => clk, reset => reset, data_input => signed(tmp_input_AGC), data_output => output_data, alpha => alpha, R=>R);

end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ROM_log is
Generic(
	length_log_ROM : natural;
	N_log_ROM : natural;
	N : natural);
Port ( 
        clk : in std_logic;
        reset : in std_logic;
        addr : in std_logic_vector(N_log_ROM-1 downto 0);
        output_LOG_ROM_data : out std_logic_vector(N-1 downto 0) := (others => '0')
);
end sinus_ROM;

architecture Behavioral of ROM_log is
type LOG_ROM is array (0 to length_log_ROM-1) of signed(N-1 downto 0);

constant ROM : LOG_ROM :=
(
0
1
2
4
5
7
8
9
11
12
13
15
16
17
19
20
21
23
24
25
26
28
29
30
31
32
34
35
36
37
38
40
41
42
43
44
45
46
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
66
67
68
69
70
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
86
87
88
89
90
91
92
93
94
94
95
96
97
98
99
100
100
101
102
103
104
104
105
106
107
108
109
109
110
111
112
112
113
114
115
116
116
117
118
119
119
120
121
122
122
123
124
125
125
126
127
);
begin
process(clk,reset)
begin
if reset = '1' then
     output_LOG_ROM_data <= (others => '0');
elsif rising_edge(clk) then
    output_LOG_ROM_data <= std_logic_vector(LOG_ROM(TO_INTEGER(UNSIGNED(addr))));
end if;
end process;

end Behavioral;

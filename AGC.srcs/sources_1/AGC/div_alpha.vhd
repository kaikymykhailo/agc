library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity div_alpha is
	GENERIC(
		N : natural
	);
	PORT( 
		A : in std_logic_vector(N-1 downto 0);
		B : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end div_alpha;

architecture Behavioral of div_alpha is
begin
	process(A,B)
	variable result : signed(2*N-1 downto 0);
	begin
	result := SIGNED(A) * SIGNED(B);
	C <= std_logic_vector(result(2*N-1 downto N));
	end process;
end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity multiply_Z_buffer_and_input_signal is
	GENERIC(
		N : natural
	);
	PORT( 
		clk : in std_logic;
		reset : in std_logic;
		A : in std_logic_vector(N-1 downto 0);
		B : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end multiply_Z_buffer_and_input_signal;

architecture Behavioral of multiply_Z_buffer_and_input_signal is
	constant INT_WL : natural := 4; --Integer part of input gain
begin
	process(A,B)
	variable tmp_B : std_logic_vector(N-1 downto 0);
	variable tmp_result : signed(N*2-1 downto 0);
	begin
	tmp_B := std_logic_vector(SHIFT_RIGHT(unsigned(B),1));
	tmp_result := signed(tmp_B) * signed(A);
	C <= std_logic_vector(tmp_result(N*2-1-INT_WL downto N-INT_WL));
	end process;

end Behavioral;

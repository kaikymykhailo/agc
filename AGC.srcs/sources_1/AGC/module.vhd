library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity module is
	GENERIC(
		N : natural
	);
	PORT( 
		reset : in std_logic;
		A : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end module;

architecture Behavioral of module is
begin

process(A, reset) 
variable result : signed(2*N-1 downto 0);
begin
if signed(A) >= TO_SIGNED(0,N) then
	result(N-1 downto 0) := signed(A);
else
	if signed(A) = TO_SIGNED(-32768, N) then
		result := (signed(A) + TO_SIGNED(-1,N))* TO_SIGNED(-1,N);
	else
		result := signed(A) * TO_SIGNED(-1,N);
	end if;
end if;
C <= std_logic_vector(result(N-1 downto 0));
end process;
end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity AGC is
Generic(N : natural);
Port ( 
	clk : in std_logic;
	reset : in std_logic;
	data_input : in signed(N-1 downto 0);
	R : in signed(N-1 downto 0);
	alpha : in unsigned(N-1 downto 0);
	data_output : out signed(N-1 downto 0)
);
end AGC;

architecture Behavioral of AGC is

component multiply_Z_buffer_and_input_signal
	GENERIC(
		N : natural
	);
	PORT( 
		clk : in std_logic;
		reset : in std_logic;
		A : in std_logic_vector(N-1 downto 0);
		B : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end component;

component div_alpha
	GENERIC(
		N : natural
	);
	PORT( 
		A : in std_logic_vector(N-1 downto 0);
		B : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end component;

component Z_sum_Z
	GENERIC(
		N : natural
	);
	PORT( 
		reset : in std_logic;
		A : in std_logic_vector(N-1 downto 0);
		B : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end component;

component sub_R
	GENERIC(
		N : natural
	);
	PORT( 
		reset : in std_logic;
		A : in std_logic_vector(N-1 downto 0);
		R : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end component;

component module
	GENERIC(
		N : natural
	);
	PORT( 
		reset : in std_logic;
		A : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end component;

signal Z_buffer : std_logic_vector(N-1 downto 0);
-- Temporary Signals --
signal main_multiply_to_output_and_to_module : std_logic_vector(N-1 downto 0);
signal tmp_main_multiply_to_output_and_to_module : std_logic_vector(N-1 downto 0);
signal module_to_sum_R : std_logic_vector(N-1 downto 0);
signal sum_R_to_div_alpha : std_logic_vector(N-1 downto 0);
signal div_alpha_to_sum_Z : std_logic_vector(N-1 downto 0);
signal sum_Z_to_Z : std_logic_vector(N-1 downto 0);
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- End Temporary Signals --

begin
module_operation : module Generic Map(N => N) Port Map(reset => reset, A=> tmp_main_multiply_to_output_and_to_module, C=> module_to_sum_R);
substraction_R : sub_R Generic Map(N => N) Port Map(reset => reset, A=> module_to_sum_R , R=>std_logic_vector(R), C=> sum_R_to_div_alpha);
div_alpha_to_sum_R : div_alpha Generic Map(N => N) Port Map(A=>std_logic_vector(alpha), B=> sum_R_to_div_alpha, C=>div_alpha_to_sum_Z);
sum_Z_and_Z_buffer : Z_sum_Z Generic Map(N => N) Port Map(reset => reset, B=>Z_buffer, A=> div_alpha_to_sum_Z, C=>sum_Z_to_Z);
multiply_Z_buffer_and_input : multiply_Z_buffer_and_input_signal Generic Map(N => N) Port Map(clk => clk, reset => reset, B=>Z_buffer, A=> main_multiply_to_output_and_to_module, C=> tmp_main_multiply_to_output_and_to_module);--main_multiply_to_output_and_to_module);


process(clk) 
begin
if reset = '1' then
	main_multiply_to_output_and_to_module <= std_logic_vector(data_input);
	data_output <= (others => '0');
elsif rising_edge(clk) then
	main_multiply_to_output_and_to_module <= std_logic_vector(data_input);
	data_output <= SIGNED(tmp_main_multiply_to_output_and_to_module);
end if;
end process;


process(clk)
begin
if reset = '1' then
	Z_buffer <= (others => '0');
elsif rising_edge(clk) then
	if sum_Z_to_Z >= std_logic_vector(TO_UNSIGNED(0,N)) then
		Z_buffer <= sum_Z_to_Z ;
	else
		Z_buffer <= Z_buffer;
	end if;
end if;
end process;

end Behavioral;

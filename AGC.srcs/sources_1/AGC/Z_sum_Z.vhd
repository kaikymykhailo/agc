library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Z_sum_Z is
	GENERIC(
		N : natural
	);
	PORT( 
		reset : in std_logic;
		A : in std_logic_vector(N-1 downto 0);
		B : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end Z_sum_Z;

architecture Behavioral of Z_sum_Z is
signal bla_A : std_logic_vector(N downto 0);
signal bla_B : std_logic_vector(N downto 0);
signal bla_tmp_result : signed(N+1 downto 0);
begin
	process(A,B)
	variable tmp_A : std_logic_vector(N downto 0);
	variable tmp_B : std_logic_vector(N downto 0);
	variable tmp_result : signed(N+1 downto 0);
	variable result : signed(N-1 downto 0);
	begin
	tmp_A := std_logic_vector(resize(SIGNED(A),N+1)); -- extension to 17 bits
	tmp_B := std_logic_vector(resize(UNSIGNED(B),N+1));
	tmp_result := resize(SIGNED(tmp_A), N+2) + resize(SIGNED(tmp_B), N+2);
	if tmp_result(N+1) /= tmp_result(N) then
		result(N-1 downto 0) := (others => tmp_result(N));
	elsif tmp_result(N+1) = '1' and tmp_result(N) = '1' then
		result(N-1 downto 0) := (others => '0');
	else
		result := tmp_result(N-1 downto 0);
	end if;
	C <= std_logic_vector(result);
	bla_A <= tmp_A;
	bla_B <= tmp_B;
	bla_tmp_result <= tmp_result;
	end process;
end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package LOG2_PACKAGE is
	constant BYTE : natural := 8;

	constant INPUT_DATA_BUS_LENGTH : natural := 16;
	constant OUTPUT_DATA_BUS_LENGTH : natural :=  INPUT_DATA_BUS_LENGTH * 2;
	constant OUTPUT_N_LENGTH : natural := 6;
	
	
	-- -- ROM LOG -- -- 
	constant LENGTH_ROM : natural := 129;
	constant ROM_ADDR_BUS_LENGTH : natural := 7; -- 129 ячеек
	constant ROM_OUTPUT_DATA_BUS_LENGTH : natural := 17;
end package;

package body LOG2_PACKAGE is
end package body;
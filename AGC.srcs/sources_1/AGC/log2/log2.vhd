library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.LOG2_PACKAGE.ALL;

entity log2 is
	PORT(
		clk : in std_logic;
		reset : in std_logic;
		input_data : in std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
		output_data : out std_logic_vector(OUTPUT_DATA_BUS_LENGTH-1 downto 0)
		);
end log2;

architecture RTL of log2 is

component log2_normalize_input_data
	PORT( 
		clk : in std_logic;
        reset : in std_logic;
        input : in std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
        output_n : out std_logic_vector(OUTPUT_N_LENGTH-1 downto 0);
        output : out std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0)
	);
end component;

component ROM_log
	PORT( 
    clk : in std_logic;
    reset : in std_logic;
    addr : in std_logic_vector(ROM_ADDR_BUS_LENGTH-1 downto 0);
	output_LOG_ROM_data : out std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0) := (others => '0');
	output_LOG_ROM_data_next : out std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0) := (others => '0')
	);
end component;

signal output_log2_normalize_input_data :  std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
signal N : std_logic_vector(OUTPUT_N_LENGTH-1 downto 0);

signal high_byte  : std_logic_vector(BYTE-1 downto 0);
signal ROM_index : std_logic_vector(BYTE-1 downto 0);
signal ROM_output_one : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
signal ROM_output_two : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
signal R : std_logic_vector(BYTE-1 downto 0);


begin
module_normalize: log2_normalize_input_data Port Map(clk => clk, reset => reset, input => input_data, output => output_log2_normalize_input_data, output_n => N);
module_log_ROM: ROM_log Port Map(clk => clk, reset => reset, addr => ROM_index(BYTE-2 downto 0), output_LOG_ROM_data => ROM_output_one, output_LOG_ROM_data_next => ROM_output_two);

high_byte <= output_log2_normalize_input_data(INPUT_DATA_BUS_LENGTH-1 downto INPUT_DATA_BUS_LENGTH - BYTE);
R <= output_log2_normalize_input_data(BYTE-1 downto 0);

ROM_index_proc: process(reset, N)
begin
if reset = '1' then
	ROM_index <= (others => '0');
else
	ROM_index <= std_logic_vector(unsigned(high_byte) - TO_UNSIGNED(128, BYTE));
end if;
end process;

interpolate: process(clk,reset)
variable temporary_output_data : std_logic_vector(OUTPUT_DATA_BUS_LENGTH-1 downto 0);
variable temp_output : std_logic_vector(OUTPUT_DATA_BUS_LENGTH downto 0);
variable temp_one : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
variable temp_two : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
variable sum_one : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
variable sum_two : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
variable temp_sum_one : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH downto 0);
variable temp_sum_two : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH downto 0);
variable mult : std_logic_vector(2*ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
begin
temp_sum_one := std_logic_vector(resize(unsigned(temp_one),ROM_OUTPUT_DATA_BUS_LENGTH+1)  + resize(unsigned(N),ROM_OUTPUT_DATA_BUS_LENGTH+1));
	if temp_sum_one(ROM_OUTPUT_DATA_BUS_LENGTH) /= temp_sum_one(ROM_OUTPUT_DATA_BUS_LENGTH-1) then
		sum_one(ROM_OUTPUT_DATA_BUS_LENGTH-1) := temp_sum_one(ROM_OUTPUT_DATA_BUS_LENGTH);
		sum_one(ROM_OUTPUT_DATA_BUS_LENGTH-2 downto 0) := (others => temp_sum_one(ROM_OUTPUT_DATA_BUS_LENGTH-1));
	else
		sum_one := temp_sum_one(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
	end if;
	temp_sum_two := std_logic_vector(resize(unsigned(temp_one),ROM_OUTPUT_DATA_BUS_LENGTH+1)  + resize(unsigned(temp_two),ROM_OUTPUT_DATA_BUS_LENGTH+1));
	if temp_sum_two(ROM_OUTPUT_DATA_BUS_LENGTH) /= temp_sum_two(ROM_OUTPUT_DATA_BUS_LENGTH-1) then
		sum_two(ROM_OUTPUT_DATA_BUS_LENGTH-1) := temp_sum_two(ROM_OUTPUT_DATA_BUS_LENGTH);
		sum_two(ROM_OUTPUT_DATA_BUS_LENGTH-2 downto 0) := (others => temp_sum_two(ROM_OUTPUT_DATA_BUS_LENGTH-1));
	else
		sum_two := temp_sum_two(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0);
	end if;
	
	mult := std_logic_vector(unsigned(sum_one) * unsigned(sum_two));
	
	temp_output := std_logic_vector(unsigned(mult(2*ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 2*ROM_OUTPUT_DATA_BUS_LENGTH-OUTPUT_DATA_BUS_LENGTH-1)) + resize(unsigned(sum_one),OUTPUT_DATA_BUS_LENGTH+1));
	if temp_output(OUTPUT_DATA_BUS_LENGTH) /= temp_output(OUTPUT_DATA_BUS_LENGTH-1) then
		temporary_output_data(OUTPUT_DATA_BUS_LENGTH-1) := temp_output(OUTPUT_DATA_BUS_LENGTH);
		temporary_output_data(OUTPUT_DATA_BUS_LENGTH-2 downto 0) := (others => temp_output(OUTPUT_DATA_BUS_LENGTH-1));
	else
		temporary_output_data := temp_output(OUTPUT_DATA_BUS_LENGTH-1 downto 0);
	end if;
if reset = '1' then
	output_data <= (others => '0');
	temporary_output_data := (others => '0');
elsif rising_edge(clk) then
		temp_one := ROM_output_one;
		temp_two := ROM_output_two;
		output_data <= temporary_output_data;
end if;
end process;

end RTL;


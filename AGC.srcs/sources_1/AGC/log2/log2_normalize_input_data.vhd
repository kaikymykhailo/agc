library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.LOG2_PACKAGE.ALL;

entity log2_normalize_input_data is
	PORT( 
		clk : in std_logic;
		reset : in std_logic;
		input : in std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
		output_n : out std_logic_vector(OUTPUT_N_LENGTH-1 downto 0);
		output : out std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0)
	);
end log2_normalize_input_data;

architecture RTL of log2_normalize_input_data is
signal tmp_output : std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
signal res_n : std_logic_vector(OUTPUT_N_LENGTH-1 downto 0);
begin

shift: process(clk,reset)
variable temp : std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
variable shift_n : std_logic_vector (OUTPUT_N_LENGTH-1 downto 0);
variable tmp_shift_n : std_logic_vector (OUTPUT_N_LENGTH-2 downto 0);
begin
if reset = '1' then
    tmp_output <= (others => '0');
	tmp_shift_n  := (others => '0');
	shift_n := (others => '0');
    temp := (others => '0');
    res_n <= (others => '0');
elsif rising_edge(clk) then
    temp := input;
    for i in 0 to INPUT_DATA_BUS_LENGTH-1 loop
	   if temp(INPUT_DATA_BUS_LENGTH-1) = '0' then
		  temp := std_logic_vector(SHIFT_LEFT(UNSIGNED(temp),1));
		  tmp_shift_n := std_logic_vector(unsigned(tmp_shift_n) + to_unsigned(1,OUTPUT_N_LENGTH-1));
	   elsif temp(INPUT_DATA_BUS_LENGTH-1) = '1' then
	       shift_n := std_logic_vector(resize(UNSIGNED(tmp_shift_n),OUTPUT_N_LENGTH));
		  exit;
	   end if;
    end loop;
	shift_n := std_logic_vector(resize(UNSIGNED(tmp_shift_n),OUTPUT_N_LENGTH));
    tmp_shift_n := (others => '0');
    tmp_output(INPUT_DATA_BUS_LENGTH-1 downto 0) <= temp;
	res_n <= shift_n;
	res_n <= std_logic_vector(TO_SIGNED(INPUT_DATA_BUS_LENGTH,OUTPUT_N_LENGTH) - TO_SIGNED(9,OUTPUT_N_LENGTH) - SIGNED(shift_n)); -- 9 length fractional part
end if;
end process;

process(clk, reset)
begin
if reset = '1' then
	output <= (others => '0');
	output_n <= (others => '0');
elsif rising_edge(clk) then
	output_n <= res_n;
	output <= tmp_output;
end if;
end process;

end RTL;
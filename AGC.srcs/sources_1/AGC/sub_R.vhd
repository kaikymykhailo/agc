library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sub_R is
	GENERIC(
		N : natural
	);
	PORT( 
		reset : in std_logic;
		A : in std_logic_vector(N-1 downto 0);
		R : in std_logic_vector(N-1 downto 0);
		C : out std_logic_vector(N-1 downto 0)
	);
end sub_R;

architecture Behavioral of sub_R is
begin
	process(A,R)
	variable result : signed(N-1 downto 0);
	variable tmp_result : signed(N downto 0);
	begin
	result := (others => '0');
	tmp_result := resize(SIGNED(R), N+1) - resize(SIGNED(A), N+1);
	if tmp_result(N) /= tmp_result(N-1) then
		result(N-1) := tmp_result(N);
		result(N-2 downto 0) := (others => tmp_result(N-1));
	else
		result := tmp_result(N-1 downto 0);
	end if;
	C <= std_logic_vector(result);
	end process;
end Behavioral;

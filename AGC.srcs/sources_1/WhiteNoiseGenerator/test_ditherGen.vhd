library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;

entity test_ditherGEN is
end test_ditherGEN;

architecture Behavioral of test_ditherGEN is

-- Constants -- 
constant OUTPUT_WIDTH : natural := 16;
constant N : natural := 16;
constant clk_period : time := 10 ns;

constant outputfilename : string := "dither_output.txt";

component DitherGen
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_208_0                       :   IN    std_logic;
        validIn                           :   IN    std_logic;
        dither                            :   OUT   std_logic_vector(3 DOWNTO 0)  -- ufix4
        );
end component;

-- Open Files -- 
file output_file : TEXT open write_mode  is outputfilename;

-- End open files -- 

-- Signals --
signal clk : std_logic := '0';
signal reset : std_logic := '0';
signal validIn : std_logic := '1';
signal dither : std_logic_vector(3 DOWNTO 0);  -- ufix4
signal enb_1_208_0 : std_logic := '1' ;

begin

module_ditherGen: ditherGen
Port Map(clk => clk, reset => reset, validIn => validIn,enb_1_208_0 => enb_1_208_0, dither => dither);

clk_process: process
begin
clk <= '0';
wait for 10 ns;
clk <= '1';
wait for 10 ns;
end process;

file_write: process(clk)
variable l : line;
begin
if rising_edge(clk) then
    write(l,to_integer(unsigned(dither)));
    writeline(output_file,l);
end if;
end process;

uut: process
begin
reset <= '0';
wait for 20 ns;
reset <= '1';
wait for clk_period*2;
reset <= '0';
wait;
end process;

end Behavioral;

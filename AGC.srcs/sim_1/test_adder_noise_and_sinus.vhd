library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity test_adder_noise_and_sinus is
end test_adder_noise_and_sinus;

architecture Behavioral of test_adder_noise_and_sinus is

-- Constants -- 
constant  WIDTH_OUTPUT : natural := 16;
constant  WIDTH_SINUS : natural := 16;
constant  WIDTH_NOISE : natural := 16;
constant clk_period : time := 10 ns;

component adder_sinus_and_noise
Generic(
    WIDTH_OUTPUT : natural;
    WIDTH_SINUS : natural;
    WIDTH_NOISE : natural
);
PORT( 
    noise : in std_logic_vector(WIDTH_NOISE-1 downto 0);
    sinus : in std_logic_vector(WIDTH_SINUS-1 downto 0);
    output : out std_logic_vector(WIDTH_OUTPUT-1 downto 0)
);
end component;

-- Signals --
signal noise : std_logic_vector(WIDTH_OUTPUT-1 downto 0);
signal sinus : std_logic_vector(WIDTH_OUTPUT-1 downto 0);
signal output : std_logic_vector(WIDTH_OUTPUT-1 downto 0);

begin

module_adder: adder_sinus_and_noise
Generic Map(WIDTH_OUTPUT => WIDTH_OUTPUT,WIDTH_SINUS => WIDTH_SINUS,WIDTH_NOISE => WIDTH_NOISE)
Port Map(noise => noise,sinus=>sinus,output=>output);

uut: process
begin
sinus <= std_logic_vector(TO_SIGNED(1022,WIDTH_SINUS));
noise <= std_logic_vector(TO_SIGNED(1022,WIDTH_NOISE));
wait for 20 ns;
sinus <= std_logic_vector(TO_SIGNED(1,WIDTH_SINUS));
noise <= std_logic_vector(TO_SIGNED(1552,WIDTH_NOISE));
wait for 20 ns;
sinus <= std_logic_vector(TO_SIGNED(18,WIDTH_SINUS));
noise <= std_logic_vector(TO_SIGNED(162,WIDTH_NOISE));
wait for 20 ns;
wait;
end process;

end Behavioral;

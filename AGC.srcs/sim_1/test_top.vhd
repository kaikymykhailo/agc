library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;


entity test_top is
end test_top;

architecture Behavioral of test_top is

-- Constants -- 
constant N : natural := 16;
constant clk_period : time := 10 ns;
constant WIDTH_DATA_NOISE : natural := 16;

constant outputfilename : string := "AGC_output.txt";

component top
Generic(
	N : natural;
	WIDTH_DATA_NOISE : natural
);
Port ( 
    clk : in std_logic;
    reset : in std_logic;
	switch_signals : in std_logic_vector(1 downto 0);
	factor : in signed(N-1 downto 0);
	alpha : in unsigned(N-1 downto 0);
	R : in signed(N-1 downto 0);
    output_data : out signed(N-1 downto 0)
);
end component;

-- Open Files -- 
file output_file : TEXT open write_mode  is outputfilename;

-- End open files -- 

-- Signals --
signal clk : std_logic := '0';
signal reset : std_logic := '0';
signal factor : signed(N-1 downto 0);
signal switch_signals : std_logic_vector(1 downto 0);
signal R : signed(N-1 downto 0);
signal alpha : unsigned(N-1 downto 0);
signal output_data : signed(N-1 downto 0);

begin

module_top: top
Generic Map(N => N, WIDTH_DATA_NOISE => WIDTH_DATA_NOISE)
Port Map(clk => clk, reset => reset, switch_signals => switch_signals, output_data => output_data, factor => factor, R => R, alpha => alpha);

clk_process: process
begin
clk <= '0';
wait for 10 ns;
clk <= '1';
wait for 10 ns;
end process;

file_write: process(clk)
variable l : line;
begin
if rising_edge(clk) then
    write(l,to_integer(output_data));
    writeline(output_file,l);
end if;
end process;

uut: process
begin
switch_signals <= "00";
factor <= TO_SIGNED(0,N);
alpha <= TO_UNSIGNED(20,N);
R <= TO_SIGNED(10000,N);
reset <= '1';
wait for 100 ns;
factor <= TO_SIGNED(32767,N);
reset <= '0';
wait for 10000000 ns;
switch_signals <= "00";
wait for 10000000 ns;
switch_signals <= "11";
wait for 10000000 ns;
switch_signals <= "00";
wait for 10000000 ns;
switch_signals <= "11";
wait for 10000000 ns;
switch_signals <= "00";
wait for 10000000 ns;
switch_signals <= "11";
wait for 10000000 ns;
switch_signals <= "00";
wait for 10000000 ns;
switch_signals <= "11";
wait for 10000000 ns;
switch_signals <= "00";
wait for 10000000 ns;
switch_signals <= "11";
wait for 10000000 ns;
switch_signals <= "00";
wait for 10000000 ns;
switch_signals <= "11";
wait;
end process;

end Behavioral;

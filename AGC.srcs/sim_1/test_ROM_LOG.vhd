library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.LOG2_PACKAGE.ALL;

entity test_ROM_LOG is
end test_ROM_LOG;

architecture RTL of test_ROM_LOG is

component ROM_log
Port(
    clk : in std_logic;
    reset : in std_logic;
    addr : in std_logic_vector(ROM_ADDR_BUS_LENGTH-1 downto 0);
    output_LOG_ROM_data : out std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0) := (others => '0');
    output_LOG_ROM_data_next : out std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0) := (others => '0')
	);
end component;

signal clk : std_logic;
signal reset : std_logic;
signal addr : std_logic_vector(ROM_ADDR_BUS_LENGTH-1 downto 0);
signal output_LOG_ROM_data : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0) := (others => '0');
signal output_LOG_ROM_data_next : std_logic_vector(ROM_OUTPUT_DATA_BUS_LENGTH-1 downto 0) := (others => '0');

constant clk_period : time := 10 ns;
signal counter : unsigned(ROM_ADDR_BUS_LENGTH-1 downto 0)  := (others => '0');
begin

module_ROM_log: ROM_log
Port Map(clk => clk, reset => reset, addr => addr, output_LOG_ROM_data => output_LOG_ROM_data, output_LOG_ROM_data_next => output_LOG_ROM_data_next);

clocker: process
begin
clk <= '1';
wait for clk_period/2;
clk <= '0';
wait for clk_period/2;
end process;

test: process(clk)

begin
if rising_edge(clk) then
counter <= counter + TO_UNSIGNED(1, ROM_ADDR_BUS_LENGTH);
if counter >= TO_UNSIGNED(127, ROM_ADDR_BUS_LENGTH) then
	counter <= (others => '0');
end if;
addr <= std_logic_vector(counter);
end if;
end process;

end RTL;
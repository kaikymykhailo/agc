library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.LOG2_PACKAGE.ALL;

entity test_log2_normalize_input_data is
end test_log2_normalize_input_data;

architecture RTL of test_log2_normalize_input_data is

constant clk_period : time := 10 ns;

signal clk : std_logic;
signal reset : std_logic;
signal input : std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
signal output_n : std_logic_vector(OUTPUT_N_LENGTH-1 downto 0);
signal output : std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);

component log2_normalize_input_data
	PORT( 
		clk : in std_logic;
		reset : in std_logic;
		input : in std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
		output_n : out std_logic_vector(OUTPUT_N_LENGTH-1 downto 0);
		output : out std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0)
	);
end component;

begin
module_log2_norm: log2_normalize_input_data Port Map(clk => clk, reset => reset, input => input, output_n => output_n, output => output);

clk_process: process
begin
clk <= '1';
wait for 10 ns;
clk <= '0';
wait for 10 ns;
end process;

uut: process
begin
reset <= '0';
wait for clk_period*2;
reset <= '1';
wait for clk_period*2;
reset <= '0';
input <= "0000000001111111";
wait for clk_period*2;
input <= "0000000100000001";
wait for clk_period*2;
input <= "1111111111111111";
wait for clk_period*2;
input <= "0000000000000001";
wait for clk_period*2;
input <= "1000011111111111";
wait for clk_period*2;
input <= "0000000000000000";
wait for clk_period*2;
input <= "1001100111111111";
wait for clk_period*2;
input <= "0000000001001101";
wait;
end process;

end RTL;

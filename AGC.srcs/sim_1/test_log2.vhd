library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.LOG2_PACKAGE.ALL;
use std.textio.all;

entity test_log2 is
end test_log2;

architecture RTL of test_log2 is
constant clk_period : time := 10 ns;
constant inputfilename : string := "log2_input.txt";
constant outputfilename : string := "log2_output.txt";
file output_file : TEXT open write_mode  is outputfilename;
file input_file : TEXT open write_mode  is inputfilename;
component log2
	PORT( 
		clk : in std_logic;
		reset : in std_logic;
		input_data : in std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
		output_data : out std_logic_vector(OUTPUT_DATA_BUS_LENGTH-1 downto 0)
	);
end component;

signal clk : std_logic;
signal reset : std_logic;
signal input_data : std_logic_vector(INPUT_DATA_BUS_LENGTH-1 downto 0);
signal output_data : std_logic_vector(OUTPUT_DATA_BUS_LENGTH-1 downto 0);
signal counter : unsigned(INPUT_DATA_BUS_LENGTH-1 downto 0)  := (others => '0');
begin
module_log2: log2 Port Map(clk => clk, reset => reset, input_data => input_data, output_data => output_data);

file_write: process(clk)
variable l : line;
begin
if rising_edge(clk)then
    write(l,to_integer(unsigned(output_data)));
    writeline(output_file,l);
    write(l,to_integer(unsigned(input_data)));
    writeline(input_file,l);
end if;
end process;

clk_process: process
begin
clk <= '0';
wait for 10 ns;
clk <= '1';
wait for 10 ns;
end process;

uut: process
begin
reset <= '1';
wait for 10 ns;
reset <= '0';
for i in 0 to 65535 loop
wait for clk_period*2;
input_data <= std_logic_vector(TO_UNSIGNED(i, INPUT_DATA_BUS_LENGTH));
end loop;
end process;

end RTL;
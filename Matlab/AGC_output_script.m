% AGC signals %
AGC_output = fopen('D:\NTLab\AGC\AGC.sim\sim_1\behav\modelsim\AGC_output.txt','r');
AGC_output_data = fscanf(AGC_output, '%d');
subplot(2,1,1);
plot(AGC_output_data);
title('Output AGC Signal');
subplot(2,1,2);
hold on; grid on;
dft = fft(AGC_output_data);
dft = dft(1:fix(length(dft)/2) +1);
old_dft_length_dft = dft(length(dft));
old_dft1 = dft(1);
dft = dft./length(dft);
dft(1) = old_dft1/length(AGC_output_data);
dft(length(dft)) = old_dft_length_dft/length(AGC_output_data);
plot(20*log10(abs(dft)), 'b');
title('Amplitude Spectrum (dB)');